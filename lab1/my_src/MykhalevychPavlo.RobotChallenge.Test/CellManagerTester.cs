using Microsoft.VisualStudio.TestTools.UnitTesting;
using MykhalevychPavlo.RobotChallenge;
using Robot.Common;
using System.Collections.Generic;

namespace MykhalevychPavlo.RobotChallenge.Test
{
    [TestClass]
    public class CellManagerTester
    {
        [TestMethod]
        public void IsCellFreeTest()
        {
            var movingRobot = new Robot.Common.Robot()
            {
                OwnerName = "Ally",
                Position = new Position(10, 10),
                Energy = 100
            };
            var robots = new List<Robot.Common.Robot>()
            {
                new()
                {
                    OwnerName = "Ally",
                    Position = new Position(0, 0),
                    Energy = 100
                },
                new()
                {
                    OwnerName = "Enemy",
                    Position = new Position(3, 3),
                    Energy = 100
                },
            };

            Assert.AreEqual(CellRobotType.Ally, CellManager.IsCellFree(new Position(0, 0), movingRobot, robots));
            Assert.AreEqual(CellRobotType.Enemy, CellManager.IsCellFree(new Position(3, 3), movingRobot, robots));
            Assert.AreEqual(CellRobotType.None, CellManager.IsCellFree(new Position(5, 5), movingRobot, robots));
        }
    }
}
