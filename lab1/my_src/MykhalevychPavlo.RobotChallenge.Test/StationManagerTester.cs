using Microsoft.VisualStudio.TestTools.UnitTesting;
using MykhalevychPavlo.RobotChallenge;
using Robot.Common;
using System.Collections.Generic;

namespace MykhalevychPavlo.RobotChallenge.Test
{
    [TestClass]
    public class StationManagerTester
    {
        [TestMethod]
        public void IsStationFreeTest()
        {
            var movingRobot = new Robot.Common.Robot()
            {
                OwnerName = "Ally",
                Position = new Position(10, 10),
                Energy = 100
            };
            var robots = new List<Robot.Common.Robot>()
            {
                new()
                {
                    OwnerName = "Ally",
                    Position = new Position(0, 0),
                    Energy = 100
                },
                new()
                {
                    OwnerName = "Enemy",
                    Position = new Position(3, 3),
                    Energy = 100
                },
                movingRobot,
                new()
                {
                    OwnerName = "Ally",
                    Position = new Position(50, 50),
                    Energy = 100
                },
                new()
                {
                    OwnerName = "Enemy",
                    Position = new Position(20, 20),
                    Energy = 100
                },
            };
            var stations = new List<EnergyStation>()
            {
                new()
                {
                    Position = new Position(0, 0)
                },
                new()
                {
                    Position = new Position(3, 4)
                },
                new()
                {
                    Position = new Position(8, 8)
                },
                new()
                {
                    Position = new Position(51, 51)
                },
                new()
                {
                    Position = new Position(19, 19)
                },
            };

            Assert.IsFalse(StationManager.IsStationFree(stations[0], movingRobot, robots));
            Assert.IsFalse(StationManager.IsStationFree(stations[1], movingRobot, robots));
            Assert.IsTrue(StationManager.IsStationFree(stations[2], movingRobot, robots));
            Assert.IsTrue(StationManager.IsStationFree(stations[3], movingRobot, robots));
            Assert.IsTrue(StationManager.IsStationFree(stations[4], movingRobot, robots));
        }
        [TestMethod]
        public void FindNearestFreeCellAroundStationTest()
        {
            var movingRobot = new Robot.Common.Robot()
            {
                OwnerName = "Ally",
                Position = new Position(10, 10),
                Energy = 100
            };
            var robots = new List<Robot.Common.Robot>()
            {
                new()
                {
                    OwnerName = "Ally",
                    Position = new Position(4, 4),
                    Energy = 100
                },
                new()
                {
                    OwnerName = "Enemy",
                    Position = new Position(5, 5),
                    Energy = 100
                },
                movingRobot,
                new()
                {
                    OwnerName = "Enemy",
                    Position = new Position(10, 24),
                    Energy = 100
                },
            };
            var stations = new List<EnergyStation>()
            {
                new()
                {
                    Position = new Position(0, 0)
                },
                new()
                {
                    Position = new Position(4, 4)
                },
                new()
                {
                    Position = new Position(10, 25)
                },
            };

            Assert.AreEqual(new Position(1, 1), StationManager.FindNearestFreeCellAroundStation(stations[0], movingRobot, robots));
            Assert.AreEqual(new Position(5, 4), StationManager.FindNearestFreeCellAroundStation(stations[1], movingRobot, robots));
            Assert.AreEqual(new Position(11, 24), StationManager.FindNearestFreeCellAroundStation(stations[2], movingRobot, robots));
        }
    }
}
