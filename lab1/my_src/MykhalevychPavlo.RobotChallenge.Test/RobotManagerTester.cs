using Microsoft.VisualStudio.TestTools.UnitTesting;
using MykhalevychPavlo.RobotChallenge;
using Robot.Common;
using System.Collections.Generic;

namespace MykhalevychPavlo.RobotChallenge.Test
{
    [TestClass]
    public class RobotManagerTester
    {
        [TestMethod]
        public void IsNearStationTest()
        {
            var movingRobot1 = new Robot.Common.Robot()
            {
                OwnerName = "Ally",
                Position = new Position(10, 10),
                Energy = 100
            };
            var movingRobot2 = new Robot.Common.Robot()
            {
                OwnerName = "Ally",
                Position = new Position(30, 30),
                Energy = 100
            };
            var movingRobot3 = new Robot.Common.Robot()
            {
                OwnerName = "Ally",
                Position = new Position(40, 40),
                Energy = 100
            };
            var movingRobot4 = new Robot.Common.Robot()
            {
                OwnerName = "Ally",
                Position = new Position(8, 8),
                Energy = 100
            };
            var movingRobot5 = new Robot.Common.Robot()
            {
                OwnerName = "Ally",
                Position = new Position(60, 60),
                Energy = 100
            };

            var robots = new List<Robot.Common.Robot>()
            {
                movingRobot5,
                new()
                {
                    OwnerName = "Enemy",
                    Position = new Position(42, 41),
                    Energy = 100
                },
                movingRobot1,
                movingRobot2,
                movingRobot3,
                movingRobot4,
                new()
                {
                    OwnerName = "Ally",
                    Position = new Position(61, 60),
                    Energy = 100
                },
                new()
                {
                    OwnerName = "Enemy",
                    Position = new Position(60, 62),
                    Energy = 100
                },
            };

            var stations = new List<EnergyStation>()
            {
                new()
                {
                    Position = new Position(9, 9),
                    Energy = 30,
                    RecoveryRate = 20
                },
                new()
                {
                    Position = new Position(13, 13),
                    Energy = 500,
                    RecoveryRate = 20
                },
                new()
                {
                    Position = new Position(41, 41),
                    Energy = 50,
                    RecoveryRate = 20
                },
                new()
                {
                    Position = new Position(60, 61),
                    Energy = 40,
                    RecoveryRate = 20
                },
            };

            Map map = new Map();
            map.Stations = stations;

            RobotManager robotManagerNot4 = new RobotManager(movingRobot1, map, robots);
            RobotManager robotManagerNot1 = new RobotManager(movingRobot2, map, robots);
            RobotManager robotManagerNot2 = new RobotManager(movingRobot3, map, robots);
            RobotManager robotManagerNot3 = new RobotManager(movingRobot4, map, robots);
            RobotManager robotManagerNear2 = new RobotManager(movingRobot5, map, robots);

            // No station near.
            Assert.IsFalse(robotManagerNot1.IsNearStation());

            // Enemy wiil collect that's why false.
            Assert.IsFalse(robotManagerNot2.IsNearStation());

            // Ally with smaller index near station - false.
            Assert.IsFalse(robotManagerNot3.IsNearStation());

            // Station with lot of energy near so don't collect from current and move to the richest
            Assert.IsFalse(robotManagerNot4.IsNearStation());

            // My robot has smaller index so will collect.
            Assert.IsTrue(robotManagerNear2.IsNearStation());
        }

        [TestMethod]
        public void GetNextPositionTest()
        {
            var movingRobot1 = new Robot.Common.Robot()
            {
                OwnerName = "Ally",
                Position = new Position(0, 0),
                Energy = 20
            };
            var movingRobot2 = new Robot.Common.Robot()
            {
                OwnerName = "Ally",
                Position = new Position(0, 50),
                Energy = 24
            };
            var movingRobot3 = new Robot.Common.Robot()
            {
                OwnerName = "Ally",
                Position = new Position(40, 0),
                Energy = 20
            };
            var movingRobot4 = new Robot.Common.Robot()
            {
                OwnerName = "Ally",
                Position = new Position(25, 25),
                Energy = 100
            };
            var movingRobot5 = new Robot.Common.Robot()
            {
                OwnerName = "Ally",
                Position = new Position(64, 64),
                Energy = 100
            };

            var robots = new List<Robot.Common.Robot>()
            {
                movingRobot5,
                movingRobot1,
                movingRobot2,
                movingRobot3,
                movingRobot4,
            };

            var stations = new List<EnergyStation>()
            {
                new()
                {
                    Position = new Position(5, 5),
                    Energy = 30,
                    RecoveryRate = 20
                },
                new()
                {
                    Position = new Position(0, 56),
                    Energy = 50,
                    RecoveryRate = 20
                },
                new()
                {
                    Position = new Position(47, 0),
                    Energy = 50,
                    RecoveryRate = 20
                },
                new()
                {
                    Position = new Position(30, 30),
                    Energy = 40,
                    RecoveryRate = 20
                },
                new()
                {
                    Position = new Position(60, 60),
                    Energy = 40,
                    RecoveryRate = 20
                },
                new()
                {
                    Position = new Position(72, 72),
                    Energy = 700,
                    RecoveryRate = 20
                },
            };

            Map map = new Map();
            map.Stations = stations;

            RobotManager robotManager1 = new RobotManager(movingRobot1, map, robots);
            RobotManager robotManager2 = new RobotManager(movingRobot2, map, robots);
            RobotManager robotManager3 = new RobotManager(movingRobot3, map, robots);
            RobotManager robotManager4 = new RobotManager(movingRobot4, map, robots);
            RobotManager robotManager5 = new RobotManager(movingRobot5, map, robots);

            // One in diagonal.
            Assert.AreEqual(new Position(1, 1), robotManager1.GetNextPosition());

            // One in y direction.
            Assert.AreEqual(new Position(0, 51), robotManager2.GetNextPosition());

            // One in x direction.
            Assert.AreEqual(new Position(41, 0), robotManager3.GetNextPosition());

            // In one step.
            Assert.AreEqual(new Position(29, 29), robotManager4.GetNextPosition());

            // Go to richest.
            Assert.AreEqual(new Position(71, 71), robotManager5.GetNextPosition());
        }
    }
}
