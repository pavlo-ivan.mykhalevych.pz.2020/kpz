﻿using Robot.Common;
using System;

namespace MykhalevychPavlo.RobotChallenge
{
    public class DistanceHelper
    {
        public static int CalculateMoveCost(Position a, Position b)
        {
            return (int)(Math.Pow(a.X - b.X, 2) + Math.Pow(a.Y - b.Y, 2));
        }
    }
}
