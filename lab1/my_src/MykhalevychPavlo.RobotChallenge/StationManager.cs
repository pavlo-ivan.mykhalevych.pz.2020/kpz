﻿using System.Collections.Generic;
using Robot.Common;

namespace MykhalevychPavlo.RobotChallenge
{
    public class StationManager
    {
        public static bool IsStationFree(EnergyStation station, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            // Check if there already is any robot that will take energy faster the my robot.
            foreach (var robot in robots)
            {
                if (robot != movingRobot)
                {
                    if ((robot.Position.X <= station.Position.X + 1) && (robot.Position.X >= station.Position.X - 1)
                        && (robot.Position.Y <= station.Position.Y + 1) && (robot.Position.Y >= station.Position.Y - 1))
                    {
                        if (robots.IndexOf(robot) < robots.IndexOf(movingRobot))
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }
        public static Position FindNearestFreeCellAroundStation(EnergyStation station, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            Position targetCell = null;
            int minDistance = int.MaxValue;
            IList<int> posibleDisplacements = new int[] { -1, 0, 1 };

            // Move through all cells around station to find closest free cell for our robot.
            foreach (var x_change in posibleDisplacements)
            {
                foreach (var y_change in posibleDisplacements)
                {
                    Position cellIterator = new Position(station.Position.X - x_change, station.Position.Y - y_change);
                    if (CellManager.IsCellFree(cellIterator, movingRobot, robots) != CellRobotType.None)
                    {
                        continue;
                    }

                    int d = DistanceHelper.CalculateMoveCost(cellIterator, movingRobot.Position);
                    if (d < minDistance)
                    {
                        minDistance = d;
                        targetCell = cellIterator;
                    }
                }
            }
            return targetCell;
        }
    }
}
