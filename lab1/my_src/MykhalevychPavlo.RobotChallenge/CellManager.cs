﻿using System.Collections.Generic;
using Robot.Common;

namespace MykhalevychPavlo.RobotChallenge
{
    public class CellManager
    {
        public static CellRobotType IsCellFree(Position cell, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            foreach (var robot in robots)
            {
                // Check who is on cell - enemy or ally.
                if (robot != movingRobot)
                {
                    if (robot.Position == cell)
                        return robot.OwnerName == movingRobot.OwnerName ? CellRobotType.Ally : CellRobotType.Enemy;
                }
            }
            return CellRobotType.None;
        }
    }
}
