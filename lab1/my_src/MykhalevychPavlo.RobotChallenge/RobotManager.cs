﻿using System.Collections.Generic;
using System.Linq;
using Robot.Common;

namespace MykhalevychPavlo.RobotChallenge
{
    public class RobotManager
    {
        private readonly Robot.Common.Robot _movingRobot;
        private readonly Map _map;
        private readonly IList<Robot.Common.Robot> _robots;
        private readonly EnergyStation _nearestFreeStation;
        private readonly EnergyStation _nearestRichestFreeStation;
        public RobotManager(Robot.Common.Robot movingRobot, Map map, IList<Robot.Common.Robot> robots)
        {
            _movingRobot = movingRobot;
            _map = map;
            _robots = robots;
            _nearestFreeStation = FindNearestFreeStation();
            _nearestRichestFreeStation = FindNearestRichestFreeStation();
        }
        public bool IsNearStation()
        {
            // Find all station from which we can collect energy.
            IList<EnergyStation> nearestStations = _map.Stations
                .Where(station =>
                    (station.Position.X <= _movingRobot.Position.X + 1) && (station.Position.X >= _movingRobot.Position.X - 1)
                    && (station.Position.Y <= _movingRobot.Position.Y + 1) && (station.Position.Y >= _movingRobot.Position.Y - 1)).ToList();


            // If no stations near then don't collect energy.
            if (nearestStations.Count == 0)
            {
                return false;
            }

            // Iterator for station from which we can't collect any energy.
            int notUsedStationsCount = 0;
            int collectedEnergy = 0;
            foreach (var station in nearestStations)
            {
                // Find all robots that can collect energy from the current station.
                IList<int> robotsToCollect = _robots
                    .Where(robot =>
                        (robot.Position.X <= station.Position.X + 1) && (robot.Position.X >= station.Position.X - 1)
                        && (robot.Position.Y <= station.Position.Y + 1) && (robot.Position.Y >= station.Position.Y - 1) && (robot != _movingRobot))
                    .Select(robot => _robots.IndexOf(robot))
                    .ToList();

                bool currentIsUsed = true;
                // Check if our robot can collect any energy or others will be first.
                foreach (var robotIndex in robotsToCollect)
                {
                    var robot = _robots[robotIndex];
                    if (robotIndex < _robots.IndexOf(_movingRobot))
                    {
                        currentIsUsed = false;
                        ++notUsedStationsCount;
                        break;
                    }
                }
                collectedEnergy += currentIsUsed ? station.Energy : 0;
            }

            // If we can't collect energy from any stations near our robot then don't collect energy.
            if (notUsedStationsCount == nearestStations.Count)
            {
                return false;
            }

            if (collectedEnergy > 100 || _nearestRichestFreeStation == null)
            {
                return true;
            }

            Position nearestRichestFreeStationCell =
                StationManager.FindNearestFreeCellAroundStation(_nearestRichestFreeStation, _movingRobot, _robots);
            var moveCostToFreeStation = DistanceHelper.CalculateMoveCost(nearestRichestFreeStationCell, _movingRobot.Position);
            if (moveCostToFreeStation <= (_nearestRichestFreeStation.RecoveryRate + _nearestRichestFreeStation.Energy) * 0.9
                && _movingRobot.Energy - moveCostToFreeStation > 0)
            {
                return false;
            }

            // Else collect energy.
            return true;
        }
        public Position GetNextPosition()
        {
            if (_nearestFreeStation == null && _nearestRichestFreeStation == null)
            {
                return null;
            }

            // Check if station with many energy is close to the robot.
            EnergyStation targetStation = _nearestFreeStation;
            Position nearestRichestFreeStationCell =
                StationManager.FindNearestFreeCellAroundStation(_nearestRichestFreeStation, _movingRobot, _robots);
            var moveCostToFreeStation = DistanceHelper.CalculateMoveCost(nearestRichestFreeStationCell, _movingRobot.Position);
            if (moveCostToFreeStation <= (_nearestRichestFreeStation.RecoveryRate + _nearestRichestFreeStation.Energy) * 0.9
                && _movingRobot.Energy - moveCostToFreeStation > 0)
            {
                targetStation = _nearestRichestFreeStation;
            }

            return MoveToStation(_movingRobot, targetStation, _robots);
        }
        private EnergyStation FindNearestFreeStation()
        {
            EnergyStation nearest = null;
            double minCost = int.MaxValue;
            IList<EnergyStation> nearestStations = _map.Stations
                .Where(station =>
                    (station.Position.X <= _movingRobot.Position.X + 1) && (station.Position.X >= _movingRobot.Position.X - 1)
                    && (station.Position.Y <= _movingRobot.Position.Y + 1) && (station.Position.Y >= _movingRobot.Position.Y - 1)).ToList();
            var freeStations = _map.Stations
                .Where(station => StationManager.IsStationFree(station, _movingRobot, _robots) && !nearestStations.Contains(station))
                .OrderBy(station => DistanceHelper.CalculateMoveCost(
                    StationManager.FindNearestFreeCellAroundStation(station, _movingRobot, _robots), _movingRobot.Position));

            IList<Robot.Common.Robot> usedRobots = new List<Robot.Common.Robot>();
            foreach (var freeStation in freeStations)
            {
                Position nearestStationCell = StationManager.FindNearestFreeCellAroundStation(freeStation, _movingRobot, _robots);
                int cost = DistanceHelper.CalculateMoveCost(nearestStationCell, _movingRobot.Position);
                var canUseStation = true;
                foreach (var robot in _robots)
                {
                    if (robot == _movingRobot || robot.OwnerName != _movingRobot.OwnerName)
                    {
                        continue;
                    }
                    if (usedRobots.Contains(robot))
                    {
                        continue;
                    }

                    Position robot_nearestStationCell = StationManager.FindNearestFreeCellAroundStation(freeStation, robot, _robots);
                    int robot_cost = DistanceHelper.CalculateMoveCost(nearestStationCell, _movingRobot.Position);
                    if(robot_cost < cost)
                    {
                        usedRobots.Add(robot);
                        canUseStation = false;
                        break;
                    }
                }
                if (canUseStation)
                {
                    nearest = freeStation;
                    break;
                }
            }
            return nearest == null ? null : nearest;
        }
        private EnergyStation FindNearestRichestFreeStation()
        {
            EnergyStation nearest = null;
            double maxWeight = int.MinValue;
            IList<EnergyStation> nearestStations = _map.Stations
                .Where(station =>
                    (station.Position.X <= _movingRobot.Position.X + 1) && (station.Position.X >= _movingRobot.Position.X - 1)
                    && (station.Position.Y <= _movingRobot.Position.Y + 1) && (station.Position.Y >= _movingRobot.Position.Y - 1)).ToList();
            foreach (var freeStation in _map.Stations.Where(station => StationManager.IsStationFree(station, _movingRobot, _robots)))
            {
                if (nearestStations.Contains(freeStation))
                {
                    continue;
                }

                Position nearestStationCell = StationManager.FindNearestFreeCellAroundStation(freeStation, _movingRobot, _robots);
                int cost = DistanceHelper.CalculateMoveCost(nearestStationCell, _movingRobot.Position);
                double weight = ((1.0 / cost) * 0.98 + 0.02 * freeStation.Energy);
                if (weight > maxWeight)
                {
                    maxWeight = weight;
                    nearest = freeStation;
                }
            }
            return nearest == null ? null : nearest;
        }
        private Position MoveToStation(Robot.Common.Robot robot, EnergyStation station, IList<Robot.Common.Robot> robots)
        {
            Position nearestStationCell = StationManager.FindNearestFreeCellAroundStation(station, robot, robots);
            var moveCostToStation = DistanceHelper.CalculateMoveCost(nearestStationCell, robot.Position);

            // If we can get to the station in one move, do it if profit will be positive.
            if (moveCostToStation <= (station.RecoveryRate + station.Energy) * 0.9 && robot.Energy - moveCostToStation > 0)
            {
                return nearestStationCell;
            }

            // Find trajectory vector.
            var robotPosition = robot.Position;
            var stationPosition = nearestStationCell;
            int x_change = robotPosition.X < stationPosition.X ? 1 : -1;
            int y_change = robotPosition.Y < stationPosition.Y ? 1 : -1;
            if (robotPosition.X == stationPosition.X)
            {
                x_change = 0;
            }
            else if (robotPosition.Y == stationPosition.Y)
            {
                y_change = 0;
            }

            var i = 0;
            Position newPosition = new Position(robotPosition.X + x_change, robotPosition.Y + y_change);

            // If on target cell is any robot skip this cell.
            while (CellManager.IsCellFree(newPosition, robot, robots) != CellRobotType.None)
            {
                newPosition = (i % 2 == 0) ? new Position(newPosition.X, newPosition.Y + y_change)
                    : new Position(newPosition.X + x_change, newPosition.Y);
                ++i;
            }
            return newPosition;
        }
    }
}
