﻿using Robot.Common;
using System.Collections.Generic;
using System.Linq;

namespace MykhalevychPavlo.RobotChallenge
{
    public class MykhalevychPavloAlgorithm : IRobotAlgorithm
    {
        public string Author
        {
            get { return "Mykhalevych Pavlo"; }
        }
        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            Robot.Common.Robot movingRobot = robots[robotToMoveIndex];
            int myRobotsCount = robots.Count(robot => robot.OwnerName == movingRobot.OwnerName);
            if ((movingRobot.Energy > 250) && (myRobotsCount < 50))
            {
                return new CreateNewRobotCommand();
            }
            if ((movingRobot.Energy > 270) && (myRobotsCount < 100))
            {
                return new CreateNewRobotCommand();
            }

            RobotManager movingRobotManager = new RobotManager(movingRobot, map, robots);
            if (movingRobotManager.IsNearStation())
            {
                return new CollectEnergyCommand();
            }

            Position nextPosition = movingRobotManager.GetNextPosition();
            if (nextPosition == null)
            {
                return null;
            }

            return new MoveCommand() { NewPosition = nextPosition };
        }
    }
}
