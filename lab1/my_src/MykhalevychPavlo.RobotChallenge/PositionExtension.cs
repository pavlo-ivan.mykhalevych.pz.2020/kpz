﻿using Robot.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MykhalevychPavlo.RobotChallenge
{
    public static class PositionExtension
    {
        public static Position Add(this Position firstPoint, Position secondPoint)
        {
            return new Position(firstPoint.X + secondPoint.X, firstPoint.Y + secondPoint.Y);
        }
        public static Position Minus(this Position firstPoint, Position secondPoint)
        {
            return new Position(firstPoint.X - secondPoint.X, firstPoint.Y - secondPoint.Y);
        }
        public static Position Divide(this Position firstPoint, int denumerator)
        {
            return new Position(firstPoint.X / denumerator, firstPoint.Y / denumerator);
        }
        public static Position Multiply(this Position firstPoint, int value)
        {
            return new Position(firstPoint.X * value, firstPoint.Y * value);
        }
    }
}
