﻿using DBManager.Infrastracture.Commands.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace AutomationManager.Infrastructure.Commands
{
    public class LambdaCommand : CommandBase
    {
        private readonly Func<object, bool> _canExecute;
        private readonly Action<object> _execute;

        public LambdaCommand(Action<object> execute, Func<object, bool> canExecute = null)
        {
            _execute = execute ?? throw new ArgumentNullException(nameof(execute));
            _execute = execute;
        }

        public override bool CanExecute(object parameter) => _canExecute?.Invoke(parameter) ?? true;

        public override void Execute(object parameter) => _execute(parameter);
    }
}


