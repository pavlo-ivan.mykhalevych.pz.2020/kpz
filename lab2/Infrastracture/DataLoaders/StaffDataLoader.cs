﻿using DBManager.MVVM.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace DBManager.Infrastracture.DataLoaders
{
    public class StaffDataLoader
    {
        static public List<StaffInfo> GetStaffData()
        {
            SqlConnection connection =
                new SqlConnection("Data Source=TSKT-LT-028;Initial Catalog=FootballClub;Integrated Security=True;TrustServerCertificate=True");
            List<StaffInfo> staff = new List<StaffInfo>();
            connection.Open();
            string sqlQuery = "SELECT TOP 10 st.staffId, tm.personName, tm.personSurname, st.jobName, c.salary" +
                " FROM dbo.Staff st" +
                " INNER JOIN dbo.TeamMembers tm ON st.staffId = tm.personStaffId" +
                " INNER JOIN dbo.Contracts c ON st.contractId = c.contractId";

            SqlCommand sqlCommand = new SqlCommand(sqlQuery, connection);

            using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
            {
                while (sqlDataReader.Read())
                {
                    staff.Add(new StaffInfo()
                    {
                        Id = (int)sqlDataReader["staffId"],
                        Name = sqlDataReader["personName"].ToString(),
                        Surname = sqlDataReader["personSurname"].ToString(),
                        JobTitle = sqlDataReader["jobName"].ToString(),
                        Salary = (double)sqlDataReader["salary"]
                    });
                }
            }
            connection.Close();
            return staff;
        }
    }
}
