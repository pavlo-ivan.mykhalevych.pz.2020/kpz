﻿using DBManager.MVVM.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace DBManager.Infrastracture.DataLoaders
{
    public class PlayerDataLoader
    {
        static public List<PlayerInfo> GetPlayerData()
        {
            SqlConnection connection =
                new SqlConnection("Data Source=TSKT-LT-028;Initial Catalog=FootballClub;Integrated Security=True;TrustServerCertificate=True");
            List<PlayerInfo> players = new List<PlayerInfo>();
            connection.Open();
            string sqlQuery = "SELECT TOP 10  p.playerId, tm.personName, tm.personSurname, p.playerPosition, c.salary, md.injured" +
                " FROM dbo.Players p" +
                " INNER JOIN dbo.TeamMembers tm ON p.playerId = tm.personPlayerId" +
                " INNER JOIN dbo.Contracts c ON p.contractId = c.contractId" +
                " INNER JOIN dbo.MedCards md ON p.medCardId = md.medCardId";

            SqlCommand sqlCommand = new SqlCommand(sqlQuery, connection);

            using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
            {
                while (sqlDataReader.Read())
                {
                    players.Add(new PlayerInfo()
                    {
                        Id = (int)sqlDataReader["playerId"],
                        Name = sqlDataReader["personName"].ToString(),
                        Surname = sqlDataReader["personSurname"].ToString(),
                        Position = sqlDataReader["playerPosition"].ToString(),
                        Salary = (double)sqlDataReader["salary"],
                        Injured = (bool)sqlDataReader["injured"]
                    });
                }
            }
            connection.Close();
            return players;
        }
    }
}
