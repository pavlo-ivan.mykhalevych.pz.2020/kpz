﻿using DBManager.MVVM.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace DBManager.Infrastracture.DataLoaders
{
    public class AchievementsDataLoader
    {
        static public List<PlayerAchievements> GetAchievementsData()
        {
            SqlConnection connection =
                new SqlConnection("Data Source=TSKT-LT-028;Initial Catalog=FootballClub;Integrated Security=True;TrustServerCertificate=True");
            List<PlayerAchievements> playerAchievements = new List<PlayerAchievements>();
            connection.Open();
            string sqlQuery = "SELECT TOP 10  playerId, tm.personName, tm.personSurname, goals, awards" +
                " FROM dbo.Achievements ach" +
                " INNER JOIN dbo.TeamMembers tm ON ach.playerId = tm.personPlayerId";

            SqlCommand sqlCommand = new SqlCommand(sqlQuery, connection);

            using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
            {
                while (sqlDataReader.Read())
                {
                    playerAchievements.Add(new PlayerAchievements()
                    {
                        Id = (int)sqlDataReader["playerId"],
                        Name = sqlDataReader["personName"].ToString(),
                        Surname = sqlDataReader["personSurname"].ToString(),
                        Goals = (int)sqlDataReader["goals"],
                        Awards = (int)sqlDataReader["awards"]
                    });
                }
            }
            connection.Close();
            return playerAchievements;
        }
    }
}
