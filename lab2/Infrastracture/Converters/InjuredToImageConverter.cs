﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace DBManager.Infrastracture.Converters
{
    public class InjuredToImageConverter : IValueConverter
    {
        Dictionary<bool, BitmapImage> cache = new Dictionary<bool, BitmapImage>();
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var injured = (bool)value;
            if (!cache.ContainsKey(injured))
            {
                var uri = new Uri(Path.GetFullPath(string.Format(@"../../../Images/PlayerState/injured_{0}.png", injured ? "yes" : "no")));
                var img = new BitmapImage(uri);
                cache.Add(injured, img);
            }
            return cache[injured];
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
