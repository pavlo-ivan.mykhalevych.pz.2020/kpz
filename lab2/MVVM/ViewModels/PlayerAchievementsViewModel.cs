﻿using System;
using System.Collections.Generic;
using System.Text;
using DBManager.Infrastracture.DataLoaders;
using DBManager.MVVM.Models;
using DBManager.MVVM.ViewModels.Base;

namespace DBManager.MVVM.ViewModels
{
    public class PlayerAchievementsViewModel : ViewModelBase
    {
        private List<PlayerAchievements> _achievements;
        public List<PlayerAchievements> Achievements { get => _achievements; set => Set(ref _achievements, value); }

        public PlayerAchievementsViewModel()
        {
            Achievements = AchievementsDataLoader.GetAchievementsData();
        }
    }
}
