﻿using System;
using System.Collections.Generic;
using System.Text;
using DBManager.Infrastracture.DataLoaders;
using DBManager.MVVM.Models;
using DBManager.MVVM.ViewModels.Base;

namespace DBManager.MVVM.ViewModels
{
    public class StaffViewModel : ViewModelBase
    {
        private List<StaffInfo> _staff;
        public List<StaffInfo> Staff { get => _staff; set => Set(ref _staff, value); }

        public StaffViewModel()
        {
            Staff = StaffDataLoader.GetStaffData();
        }
    }
}
