﻿using DBManager.Infrastracture.DataLoaders;
using DBManager.MVVM.Models;
using DBManager.MVVM.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBManager.MVVM.ViewModels
{
    public class PlayerViewModel : ViewModelBase
    {
        private List<PlayerInfo> _players;
        public List<PlayerInfo> Players { get => _players; set => Set(ref _players, value); }

        public PlayerViewModel()
        {
            Players = PlayerDataLoader.GetPlayerData();
        }
    }
}
