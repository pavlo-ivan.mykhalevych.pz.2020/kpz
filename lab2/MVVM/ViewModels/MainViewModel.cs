﻿using DBManager.MVVM.ViewModels.Base;
using DBManager.MVVM.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using AutomationManager.Infrastructure.Commands;

namespace DBManager.MVVM.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        private object _currentView;
        public object CurrentView
        {
            get { return _currentView; }
            set { Set(ref _currentView, value); }
        }

        public PlayerViewModel PlayerViewModel { get; set; }
        public StaffViewModel StaffViewModel { get; set; }
        public PlayerAchievementsViewModel PlayerAchievementsViewModel { get; set; }

        public LambdaCommand SwitchToPlayersCommand { get; set; }
        public LambdaCommand SwitchToStaffCommand { get; set; }
        public LambdaCommand SwitchToAchievementsCommand { get; set; }

        public MainViewModel()
        {
            PlayerViewModel = new PlayerViewModel();
            StaffViewModel = new StaffViewModel();
            PlayerAchievementsViewModel = new PlayerAchievementsViewModel();

            SwitchToPlayersCommand = new LambdaCommand(o => CurrentView = PlayerViewModel);
            SwitchToStaffCommand = new LambdaCommand(o => CurrentView = StaffViewModel);
            SwitchToAchievementsCommand = new LambdaCommand(o => CurrentView = PlayerAchievementsViewModel);

            CurrentView = PlayerViewModel;
        }

    }
}
