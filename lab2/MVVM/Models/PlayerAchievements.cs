﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBManager.MVVM.Models
{
    public class PlayerAchievements
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public int Goals { get; set; }
        public int Awards { get; set; }
    }
}
