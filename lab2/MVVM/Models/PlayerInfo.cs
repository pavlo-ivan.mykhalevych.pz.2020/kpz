﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBManager.MVVM.Models
{
    public class PlayerInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Position { get; set; }
        public double Salary { get; set; }
        public bool Injured { get; set; }
    }
}
